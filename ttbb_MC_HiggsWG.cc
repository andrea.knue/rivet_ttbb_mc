#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"

namespace Rivet {

  // Rivet routine for tt+bb studies, based on the routine ATLAS_2018_I1705857

  class ttbb_MC_HiggsWG : public Analysis {
  public:
    /// Constructor
    /// @brief ttbb at 13 TeV
    DEFAULT_RIVET_ANALYSIS_CTOR(ttbb_MC_HiggsWG);

    void init() {
      // Eta ranges
      Cut eta_full = (Cuts::abseta < 5.0);
      // Lepton cuts
      Cut lep_cuts27 = (Cuts::abseta < 2.5) && (Cuts::pT >= 27*GeV);
      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      PromptFinalState photons(eta_full && Cuts::abspid == PID::PHOTON, true);

      // Projection to find the electrons
      PromptFinalState electrons(eta_full && Cuts::abspid == PID::ELECTRON, true);

      // Projection to find the muons
      PromptFinalState muons(eta_full && Cuts::abspid == PID::MUON, true);

      DressedLeptons dressedelectrons27(photons, electrons, 0.1, lep_cuts27, true);
      DressedLeptons dressedmuons27(photons, muons, 0.1, lep_cuts27, true);

      declare(dressedelectrons27, "elecs");
      declare(dressedmuons27,     "muons");

      // From here on we are just setting up the jet clustering
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);

      PromptFinalState jet_photons(eta_full && Cuts::abspid == PID::PHOTON, false);
      DressedLeptons all_dressed_electrons(jet_photons, electrons, 0.1, eta_full, true);
      DressedLeptons all_dressed_muons(jet_photons, muons, 0.1, eta_full, true);

      VetoedFinalState vfs(fs);
      vfs.addVetoOnThisFinalState(all_dressed_electrons);
      vfs.addVetoOnThisFinalState(all_dressed_muons);
      vfs.addVetoOnThisFinalState(neutrinos);

      //      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::DECAY, JetAlg::Invisibles::DECAY);
      //     FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::DECAY_MUONS, JetAlg::DECAY_INVISIBLES);
      
      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::DECAY, JetAlg::Invisibles::DECAY);

      declare(jets, "jets");

      book(_h["avg_dr_bb_ljet_3b"],  "avg_dr_bb_ljet_3b",  20,  0.0,    6.0);
      book(_h["min_dr_bb_ljet_3b"],    "min_dr_bb_ljet_3b",    20,  0.0,    6.0);
      book(_h["mbb_maxpt_ljet_3b"],    "mbb_maxpt_ljet_3b",    10,  0.0,  470.0);
      book(_h["mbb_maxpt_ljet_3b_scalar"], "mbb_maxpt_ljet_3b_scalar",    10,  0.0,  470.0);
      book(_h["mbb_mindr_ljet_3b"],    "mbb_mindr_ljet_3b",    10,  0.0,  470.0);
      book(_h["ht_bjets_ljet_3b"],     "ht_bjets_ljet_3b",     14,  0.0, 1400.0);
      book(_h["ht_lightjets_ljet_3b"], "ht_lightjets_ljet_3b", 14,  0.0, 1400.0);
      book(_h["njets_ljet_3b"],        "njets_ljet_3b",         8,  2.5,   10.5);
      book(_h["jet_eta_ljet_3b"],      "jet_eta_ljet_3b",      20, -2.7,    2.7);

      book(_h["avg_dr_bb_ljet_4b"],    "avg_dr_bb_ljet_4b",    20,  0.0,    6.0);
      book(_h["min_dr_bb_ljet_4b"],    "min_dr_bb_ljet_4b",    20,  0.0,    6.0);
      book(_h["mbb_maxpt_ljet_4b"],    "mbb_maxpt_ljet_4b",    32,  0.0,  500.0);
      book(_h["mbb_maxpt_ljet_4b_scalar"], "mbb_maxpt_ljet_4b_scalar",    32,  0.0,  500.0);
      book(_h["mbb_mindr_ljet_4b"],    "mbb_mindr_ljet_4b",    10,  0.0,  470.0);
      book(_h["ht_bjets_ljet_4b"],     "ht_bjets_ljet_4b",     14,  0.0, 1400.0);
      book(_h["ht_lightjets_ljet_4b"], "ht_lightjets_ljet_4b", 14,  0.0, 1400.0);
      book(_h["njets_ljet_4b"],        "njets_ljet_4b",         8,  2.5,   10.5);
      book(_h["jet_eta_ljet_4b"],      "jet_eta_ljet_4b",      20, -2.7,    2.7);

      book(_h["avg_dr_bb_emu_3b"],     "avg_dr_bb_emu_3b",     20,  0.0,    6.0);
      book(_h["min_dr_bb_emu_3b"],     "min_dr_bb_emu_3b",     20,  0.0,    6.0);
      book(_h["mbb_maxpt_emu_3b"],     "mbb_maxpt_emu_3b",     10,  0.0,  470.0);
      book(_h["mbb_mindr_emu_3b"],     "mbb_mindr_emu_3b",     10,  0.0,  470.0);
      book(_h["ht_bjets_emu_3b"],      "ht_bjets_emu_3b",      14,  0.0, 1400.0);
      book(_h["ht_lightjets_emu_3b"],  "ht_lightjets_emu_3b",  14,  0.0, 1400.0);
      book(_h["njets_emu_3b"],         "njets_emu_3b",          9,  1.5,   10.5);
      book(_h["jet_eta_emu_3b"],       "jet_eta_emu_3b",       20, -2.7,    2.7);

      book(_h["avg_dr_bb_emu_4b"],     "avg_dr_bb_emu_4b",     20,  0.0,    6.0);
      book(_h["min_dr_bb_emu_4b"],     "min_dr_bb_emu_4b",     20,  0.0,    6.0);
      book(_h["mbb_maxpt_emu_4b"],     "mbb_maxpt_emu_4b",     10,  0.0,  470.0);
      book(_h["mbb_mindr_emu_4b"],     "mbb_mindr_emu_4b",     10,  0.0,  470.0);
      book(_h["ht_bjets_emu_4b"],      "ht_bjets_emu_4b",      14,  0.0, 1400.0);
      book(_h["ht_lightjets_emu_4b"],  "ht_lightjets_emu_4b",  14,  0.0, 1400.0);
      book(_h["njets_emu_4b"],         "njets_emu_4b",          9,  1.5,   10.5);
      book(_h["jet_eta_emu_4b"],       "jet_eta_emu_4b",       20, -2.7,    2.7);


      book(_h["nbjets_ljet_3b"],        "nbjets_ljet_3b",         8,  2.5,   10.5);
      book(_h["nbjets_ljet_4b"],        "nbjets_ljet_4b",         8,  2.5,   10.5);

      

    }


    void analyze(const Event& event) {

      vector<DressedLepton> leptons;
      for (auto &lep : apply<DressedLeptons>(event, "muons").dressedLeptons()) { leptons.push_back(lep); }
      for (auto &lep : apply<DressedLeptons>(event, "elecs").dressedLeptons()) { leptons.push_back(lep); }

      const Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);
      for (const auto& jet : jets) {
        ifilter_discard(leptons, [&](const DressedLepton& lep) { return deltaR(jet, lep) < 0.4; });
      }

      Jets bjets;
      Jets lightjets;
      for (const Jet& jet : jets) {
        if (jet.bTagged(Cuts::pT >= 5*GeV))  bjets += jet;
	else lightjets += jet;
      }

      if(bjets.size() < 3) vetoEvent;

      

      size_t njets = jets.size();
      size_t nbjets = bjets.size();
      size_t nlightjets = lightjets.size();

      // Evaluate basic event selection
      bool pass_ljets = (leptons.size() == 1 && leptons[0].pT() > 27*GeV);

      bool pass_emu =
        // 2 leptons > 27 GeV
        (leptons.size() == 2) &&
        (leptons[0].pT() > 27*GeV && leptons[1].pT() > 27*GeV) &&
        // emu events
        ((leptons[0].abspid() == 11 && leptons[1].abspid() == 13) ||
         (leptons[0].abspid() == 13 && leptons[1].abspid() == 11)) &&
        // opposite charge
        (leptons[0].charge() != leptons[1].charge());

      // If we don't have exactly 1 or 2 leptons then veto the event
      if (!pass_emu && !pass_ljets)  vetoEvent;

      //std::cout << "Hier 1" << std::endl;

      /*if (pass_emu) {
        if (nbjets == 3)  fill("nbjets_emu", nbjets - 1);
      }

      if (pass_ljets) {
      }*/

      //      std::cout << "Pass selection = " << pass_emu << "\t" << pass_ljets << std::endl;

      //      if (pass_emu && (nbjets < 3 || njets < 2))    vetoEvent;
      if (pass_ljets && (nbjets < 3 || njets <= 3))  vetoEvent;

      //std::cout<< "Hier 2" << std::endl;

      double hthad = sum(jets, pT, 0.0);

      //std::cout<< "Hier 2a" << std::endl;

      double ht = sum(leptons, pT, hthad);
      //std::cout<< "Hier 2b" << std::endl;
      double hthadb = sum(bjets, pT, 0.0);
      //std::cout<< "Hier 2c" << std::endl;
      double hthadlight = sum(lightjets, pT, 0.0);
      //std::cout<< "Hier 2d = " << nbjets << "\t" << bjets.size() << std::endl;
      double dr_leading = deltaR(bjets[0], bjets[1]);

      //std::cout<< "Hier 2e" << std::endl;
      double avg_dr  = 0.0;
      size_t nr_perm = 0;

      size_t ind1 = -1; size_t ind2 = -1; double mindr = 999.;
      size_t ind1_max = -1; size_t ind2_max = -1; double maxpt = -999.;

      //      if(bjets.size() >= 4)
      std::cout << "========================================================================================================" << std::endl;
      //std::cout<< "Hier 3" << std::endl;

      for (size_t i = 0; i < bjets.size(); ++i) {

	//	std::cout << "bjet pt =  " << i << "\t" << (bjets[i].momentum()).pT() << std::endl;

      }


      for (size_t i = 0; i < bjets.size(); ++i) {

        for (size_t j = 0; j < bjets.size(); ++j) {

          if (i == j)  continue;
          double dr = deltaR(bjets[i], bjets[j]);
	  double pt = (bjets[i].momentum() + bjets[j].momentum()).pT();
	  
	  if(bjets.size() > 3)
	    std::cout << dr << "\t" << (bjets[i].momentum() + bjets[j].momentum()).mass() << "\t" << (bjets[i].momentum()).pT() + (bjets[j].momentum()).pT() << std::endl;

	  //  if(bjets.size() >= 4){
	    // std::cout << bjets[i].

	      // std::cout << "calc min dr = " << i << "\t" << j << "\t" << dr << std::endl;

	      //}
	  
	  avg_dr += dr;
	  nr_perm += 1;

          if (dr < mindr) {
            ind1 = i;
            ind2 = j;
            mindr = dr;
	    
	    // if(bjets.size() >= 4)
	    // std::cout << "mindr = " << mindr << "\t" << (bjets[i].momentum()+bjets[j].momentum()).mass()/GeV << std::endl;

          }
	  if (pt > maxpt){
	    ind1_max = i;
	    ind2_max = j;
	    maxpt = pt;
	  }
        }
      }

      //std::cout<< "Hier 4" << std::endl;

      avg_dr = avg_dr/nr_perm;

      FourMomentum jsum = bjets[ind1_max].momentum() + bjets[ind2_max].momentum();
      FourMomentum jsum_scalar = bjets[0].momentum() + bjets[1].momentum();
      FourMomentum bb_closest = bjets[ind1].momentum() + bjets[ind2].momentum();
      double dr_closest = deltaR(bjets[ind1], bjets[ind2]);

      //     std::cout << "written = " << bb_closest.mass() << "\t" << jsum_scalar.mass() << std::endl;

      if (pass_ljets && njets >= 4) {

	std::cout << "Lepton info" << leptons.size() << "\t" << leptons[0].pT() << "\t" << leptons[0].eta() << std::endl;
      
	if(nbjets==3){

	  _h["avg_dr_bb_ljet_3b"] -> fill(avg_dr);
	  _h["min_dr_bb_ljet_3b"] -> fill(dr_closest);
	  _h["mbb_maxpt_ljet_3b"] -> fill(jsum.mass());
	  _h["mbb_maxpt_ljet_3b_scalar"] -> fill(jsum_scalar.mass());
	  _h["mbb_mindr_ljet_3b"] -> fill(bb_closest.mass());
	  _h["ht_bjets_ljet_3b"]  -> fill(hthadb/GeV);
	  _h["ht_lightjets_ljet_3b"] -> fill(hthadlight/GeV);
	  _h["njets_ljet_3b"] -> fill(njets);
	  _h["nbjets_ljet_3b"] -> fill(nbjets);

	  for (size_t i = 0; i < jets.size(); ++i)
	    _h["jet_eta_ljet_3b"] -> fill(jets[i].eta());
	  
	}

	else if(nbjets>=4){

	  std::cout << "mbb max pt = " << jsum_scalar.mass() << std::endl; 


          _h["avg_dr_bb_ljet_4b"] -> fill(avg_dr);
          _h["min_dr_bb_ljet_4b"] -> fill(dr_closest);
          _h["mbb_maxpt_ljet_4b"] -> fill(jsum.mass());
	  _h["mbb_maxpt_ljet_4b_scalar"] -> fill(jsum_scalar.mass());
          _h["mbb_mindr_ljet_4b"] -> fill(bb_closest.mass());
          _h["ht_bjets_ljet_4b"]  -> fill(hthadb/GeV);
          _h["ht_lightjets_ljet_4b"] -> fill(hthadlight/GeV);
          _h["njets_ljet_4b"] -> fill(njets);
	  _h["nbjets_ljet_4b"] -> fill(nbjets);

          for (size_t i = 0; i < jets.size(); ++i)
            _h["jet_eta_ljet_4b"] -> fill(jets[i].eta());

        }
	
      }

      //std::cout<< "Hier 5" << std::endl;

      if (pass_emu && njets >= 3) {
	
	if(nbjets==3){

          _h["avg_dr_bb_emu_3b"] -> fill(avg_dr);
          _h["min_dr_bb_emu_3b"] -> fill(dr_closest);
          _h["mbb_maxpt_emu_3b"] -> fill(jsum.mass());
          _h["mbb_mindr_emu_3b"] -> fill(bb_closest.mass());
          _h["ht_bjets_emu_3b"]  -> fill(hthadb/GeV);
          _h["ht_lightjets_emu_3b"] -> fill(hthadlight/GeV);
          _h["njets_emu_3b"] -> fill(njets);
          for (size_t i = 0; i < jets.size(); ++i)
            _h["jet_eta_emu_3b"] -> fill(jets[i].eta());

        }

        else if(nbjets>=4){

          _h["avg_dr_bb_emu_4b"] -> fill(avg_dr);
          _h["min_dr_bb_emu_4b"] -> fill(dr_closest);
          _h["mbb_maxpt_emu_4b"] -> fill(jsum.mass());
          _h["mbb_mindr_emu_4b"] -> fill(bb_closest.mass());
          _h["ht_bjets_emu_4b"]  -> fill(hthadb/GeV);
          _h["ht_lightjets_emu_4b"] -> fill(hthadlight/GeV);
          _h["njets_emu_4b"] -> fill(njets);
          for (size_t i = 0; i < jets.size(); ++i)
            _h["jet_eta_emu_4b"] -> fill(jets[i].eta());

        }

      }
    }

    void finalize() {
      // Normalise all histograms
      const double sf = crossSection() / femtobarn / sumOfWeights();
      for (auto const& h : _h) {
        scale(h.second, sf);
        //if (h.first.find("fid_xsec") != string::npos)  continue;
        //normalize(h.second, 1.0);
      }
    }

    //void fill(const string& name, const double value) {
    //  _histograms[name]->fill(value);
    //}
    
    //void book_hist(const std::string& name, unsigned int d) {
    //  book(_histograms[name], d, 1, 1);
    //}
    
  private:
    map<std::string, Histo1DPtr> _h;

  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ttbb_MC_HiggsWG);
}
