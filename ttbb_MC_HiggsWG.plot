# BEGIN PLOT /ttbb_MC_HiggsWG/*
XTwosidedTicks=1
YTwosidedTicks=1
# LegendXPos=0.7
#LogX=1
#LogY=1
LegendAlign=l
LegendXPos=0.05
LegendYPos=0.30
NormalizeToSum=1
# END PLOT

# BEGIN PLOT /ttbb_MC_HiggsWG/mbb
XLabel= $m_{b\bar{b}}$ [GeV]
YLabel= Arbitrary units
# END PLOT
